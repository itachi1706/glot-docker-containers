#!/bin/bash
shopt -s globstar
set -e

for dockerfile in **/Dockerfile; do
    tagPath=$(dirname "$dockerfile")
    imagePath=$(dirname "$tagPath")
    tag=$(basename "$tagPath")
    image=$(basename "$imagePath")
    imageName="registry.gitlab.com/itachi1706/glot-docker-containers/${image}:${tag}"

    # Build image
    (
        echo "Building $imageName"
        cd "$tagPath"
        docker build --pull --no-cache --network host -t "$imageName" . || true
    )
done
