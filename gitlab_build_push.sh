#!/bin/bash
shopt -s globstar
set -e

# We shall do this instead as due to the lack of space on the ci instance, we cannot build everything THEN push sadly

# Required arg
foldertogo=$1

echo "Building for $foldertogo"
cd "$foldertogo"

for dockerfile in **/Dockerfile; do
    (
        tag=$(dirname "$dockerfile")
        imagePath=$(basename "$PWD")
        image=$(basename "$imagePath")
        imageName="registry.gitlab.com/itachi1706/glot-docker-containers/${image}:${tag}"
    
        # Build image
        echo "Building $imageName"
        cd "$tag"
        docker build --pull --no-cache --network host -t "$imageName" . || true
    
        # Push image
        echo
        echo "Pushing $imageName"
        docker push "$imageName"
    )
done
